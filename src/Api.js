const apiKey = ''
const baseUrl = 'http://api.themoviedb.org/3/movie'

export async function fetchMovies() {
    try {
        const [popularRes, topRatedRes] = await Promise.all([
            fetch(`${baseUrl}/popular?api_key=${apiKey}`),
            fetch(`${baseUrl}/top_rated?api_key=${apiKey}`)
        ])

        const [popularJson, topRatedJson] = await Promise.all([
            popularRes.json(),
            topRatedRes.json()
        ])

        // Concat top rated and most popular.
        const movies = [...popularJson.results, ...topRatedJson.results]

        // Filter duplicates
        return movies.filter((movie, i) => movies.indexOf(movie) >= i)
    } catch (err) {
        console.error('Failed to load Movies!', err)
        return []
    }
}

export async function fetchMovieDetails(id) {
    try {
        const url = `${baseUrl}/${id}?api_key=${apiKey}`
        const res = await fetch(url)
        const json = await res.json()
        return json
    } catch (err) {
        console.error('Failed to load Movie details!', err)
        return {}
    }
}

export function getImageUrl(path, detailsPage) {

    let imageWidth = 154

    if (!detailsPage) {
        // Check the screen size to know which resolution to load.
        const appWidth = window.innerWidth
        if (appWidth > 1000)
            imageWidth = 780
        else if (appWidth > 684)
            imageWidth = 500
        else if (appWidth > 370)
            imageWidth = 342
        else if (appWidth > 308)
            imageWidth = 185
    }

    return `http://image.tmdb.org/t/p/w${imageWidth}/${path}`
}