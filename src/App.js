import React, { useState, useEffect } from 'react'
import Router from './Router'
import { fetchMovies } from './Api'
import './Styles.scss'

const App = (props) => {

  // Create local state to contain the movies.
  const [movies, setMovies] = useState([])

  useEffect(() => {
    // Fetch the movies and set them to the state.s
    (async () => setMovies(await fetchMovies()))()
  }, [])

  return (
    <div className="App">
      <Router {...props} movies={movies} setMovies={setMovies} />
    </div>
  )
}

export default App