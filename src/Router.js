import React from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import { DiscoveryView, DetailsView } from './Pages'

const Router = (props) => (
  <BrowserRouter className='Router'>
    <Switch>
      <Route path="/" exact component={() => <DiscoveryView {...props} />} />
      <Route path='/details/:id' component={() => <DetailsView {...props} />} />
      <Route render={() => <Redirect to="/" />} />
    </Switch>
  </BrowserRouter>
)

export default Router