import React from 'react'
import { withRouter, Link } from 'react-router-dom'
import { dots, arrow_back } from './assets'
import './NavBar.scss'

const NavBar = (props) => (
  <nav className='NavBar verticalCenteredRow'>
    <div className='leftSide verticalCenteredRow'>
      {
        props.match.path === '/' ?
          'Pop Movie'
          : <Link to='/' className='verticalCenteredRow'>
            <img src={arrow_back} alt='Popular Movies' />
            <span>MovieDetail</span>
          </Link>
      }
    </div>
    <div className='rightSide verticalCenteredRow'>
      <img src={dots} alt='Movies App Menu' />
    </div>
  </nav>
)

export default withRouter(NavBar)