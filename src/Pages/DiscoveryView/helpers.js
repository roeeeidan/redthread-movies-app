export const chunkArray = (array, chunkSize) => (
    [].concat.apply([], array.map((elem, i) =>
        i % chunkSize ? [] : [array.slice(i, i + chunkSize)]
    ))
)