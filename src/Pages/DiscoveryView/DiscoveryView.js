import React from 'react'
import { Link } from 'react-router-dom'
import { NavBar } from '../../Components'
import { getImageUrl } from '../../Api'
import withScrollToTop from '../withScrollToTop'
import { chunkArray } from './helpers'
import './DiscoveryView.scss'

const DiscoveryView = (props) => (
  <div className='DiscoveryView'>
    <NavBar />
    <div className='grid'>
      {
        chunkArray(props.movies, 2).map((row, i) => (
          <div className='row' key={i}>
            {
              row.map((movie, i) => (
                <Link to={`/details/${movie.id}`} className='rowItem' key={i}>
                  <img
                    src={getImageUrl(movie.poster_path)}
                    alt={movie.title}
                  />
                </Link>
              ))
            }
          </div>
        ))
      }
    </div>
  </div>
)

export default withScrollToTop(DiscoveryView)