import React, { useEffect } from 'react'

const withScrollToTop = TargetComponent => (props) => {

    useEffect(() => { window.scrollTo(0, 0) }, [])

    return (
        <TargetComponent {...props} />
    )
}

export default withScrollToTop