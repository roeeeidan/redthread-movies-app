import React, { useEffect } from 'react'
import { withRouter, Redirect } from 'react-router-dom'
import withScrollToTop from '../withScrollToTop'
import { NavBar } from '../../Components'
import { getImageUrl, fetchMovieDetails } from '../../Api'
import './DetailsView.scss'

const DetailsView = (props) => {
  const { match, movies } = props
  const { id } = match.params
  const movieIndex = movies.findIndex((movie) => movie.id === parseInt(id))
  const movie = movies[movieIndex]

  useEffect(() => {
    if (!movie || movie.hasDetails) return
    (async () => {
      const movieDetails = await fetchMovieDetails(id)
      movies[movieIndex] = { ...movie, ...movieDetails, hasDetails: true }
      props.setMovies(movies)
    })()
  })

  if (movies.length && !movie)
    return <Redirect to='/' />
  
  return (
    <div className='DetailsView'>
      <NavBar />
      {
        movie ?
          <div>
            <div className='titleWrap verticalCenteredRow'>
              {movie.title}
            </div>
            <div className='whitePage'>
              <div className='row'>
                <img className='moviePoster' src={getImageUrl(movie.poster_path, true)} alt={movie.title} />
                <div>
                  <div className='year'>
                    {(new Date(movie.release_date)).getFullYear()}
                  </div>
                  <div className='length'>
                    <i>
                      120 min
                    </i>
                  </div>
                  <div className='rating'>
                    {movie.vote_average}/10
                  </div>
                  <div className='markFavorite verticalCenteredRow'>
                    Mark as favorite
                  </div>
                </div>
              </div>
              <div className='overView'>
                {movie.overview}
              </div>
              <div className='trailersTitle'>
                Trailers:
              </div>
              <div className='trailer row'>
                <div className='playButton'></div>
                <div className='verticalCenteredRow'> Trailer 1</div>
              </div>
              <div className='trailer row'>
                <div className='playButton'></div>
                <div className='verticalCenteredRow'> Trailer 2</div>
              </div>
            </div>
          </div>
          : <span></span>
      }
    </div>
  )
}

export default withScrollToTop(withRouter(DetailsView))